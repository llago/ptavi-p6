#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Clase (y programa principal) para un servidor de eco en UDP simple."""

import socketserver
import sys
import os


class EchoHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""

    def handle(self):
        """handle."""
        bad_req = b"SIP/2.0 400 Bad Request\r\n\r\n"
        while 1:
            # Leyendo línea a línea lo que nos envía el cliente
            line = self.rfile.read()
            try:
                print("El cliente nos manda " + line.decode('utf-8'))
                MENSAJE = line.decode('utf-8')
            except UnicodeDecodeError:
                MENSAJE = ""
                break

            # Compruebo que no sea un salto de linea
            if MENSAJE != '':

                PETICION = MENSAJE.split(' ')
                allow = ['INVITE', 'ACK', 'BYE']
                check = PETICION[0] in allow
                if check:
                    if PETICION[0] == 'INVITE':
                        try:
                            # Compruebo los datos
                            direccion = PETICION[1].split(':')
                            test_dir = direccion[1].split('@')
                            test_ip = test_dir[1].split('.')

                            # Compruebo la ip
                            n = len(test_ip)
                            if test_ip[n-1] == '':
                                test_ip.remove('')

                            if len(test_dir) == 2 and len(test_ip) == 4:
                                mensaje = b"SIP/2.0 100 Trying\r\n\r\n"
                                mensaje += b"SIP/2.0 180 Ringing\r\n\r\n"
                                mensaje += b"SIP/2.0 200 OK\r\n\r\n"
                                self.wfile.write(mensaje)
                            else:
                                self.wfile.write(bad_req)
                        except IndexError:
                            self.wfile.write(bad_req)

                    elif PETICION[0] == 'ACK':
                        ejecutar = "mp32rtp -i 127.0.0.1 -p 23032 < "
                        ejecutar += sys.argv[3]
                        print("se va a ejecutar el programa")
                        os.system(ejecutar)

                    elif PETICION[0] == 'BYE':
                        self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")

                # Cuando check sea falso, una peticion a cualquier otro metodo
                else:
                    self.wfile.write(b"SIP/2.0 405 Method Not Allowed")

            # Si es un salto de linea salgo del bucle
            else:
                break

            # Si no hay más líneas salimos del bucle infinito
            if not line:
                break


if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos
    if len(sys.argv) != 4:
        sys.exit('Usage: python3 server.py IP port audio_file')
    else:
        if os.path.exists(sys.argv[3]):
            print("Listening...")
        else:
            sys.exit('Usage: python3 server.py IP port audio_file')

    PORT = sys.argv[2]
    serv = socketserver.UDPServer(('', int(PORT)), EchoHandler)
    print("Lanzando servidor UDP de eco...")
    serv.serve_forever()
