#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Programa cliente que abre un socket a un servidor."""

import socket
import sys


if __name__ == "__main__":

    if len(sys.argv) != 3:
        sys.exit('Usage: python3 client.py method receiver@IP:SIPport')
    else:
        direccion = sys.argv[2]
        direct = direccion.split('@')
        ip_port = direct[1].split(':')
        ip = ip_port[0].split('.')
        metodo = sys.argv[1]
        if len(ip) != 4:
            sys.exit('Usage: python3 client.py method receiver@IP:SIPport')

    # Dirección IP del servidor.
    SERVER = 'localhost'
    PORT = int(ip_port[1])

    # Contenido que vamos a enviar
    LINE = (metodo + " sip:" + direccion + " SIP/2.0\r\n\r\n")

    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        my_socket.connect((SERVER, PORT))

        print("Enviando: " + LINE)
        my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
        data = my_socket.recv(1024)

        print(data.decode('utf-8'))
        lista = data.decode('utf-8').split('\r\n\r\n')[0:-1]
        if lista == ["SIP/2.0 100 Trying", "SIP/2.0 180 Ringing",
                     "SIP/2.0 200 OK"]:
            LINE = ("ACK sip:" + direccion + " SIP/2.0\r\n\r\n")
            my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
            data = my_socket.recv(1024)

            LINE = ("BYE sip:" + direccion + " SIP/2.0\r\n\r\n")
            my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')

        print("Terminando socket...")

    print("Fin.")
